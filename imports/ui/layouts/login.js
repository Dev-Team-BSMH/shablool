import React from 'react';

export default ({ children }) =>
  <div id="login-layout">
    <div className="container">
      {children}
    </div>
  </div>;
